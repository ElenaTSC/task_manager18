package ru.tsk.ilina.tm.exception.empty;

import ru.tsk.ilina.tm.exception.AbstractException;

public class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error! Id is empty");
    }

    public EmptyIdException(String message) {
        super("Error! " + message + " id is empty");
    }

}
