package ru.tsk.ilina.tm.exception.empty;

import ru.tsk.ilina.tm.exception.AbstractException;

public class EmptyLoginException extends AbstractException {

    public EmptyLoginException() {
        super("Error! Login is empty");
    }

    public EmptyLoginException(String message) {
        super("Error! " + message + " login is empty");
    }

}
