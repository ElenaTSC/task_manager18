package ru.tsk.ilina.tm.exception.empty;

import ru.tsk.ilina.tm.exception.AbstractException;

public class EmptyStatusException extends AbstractException {

    public EmptyStatusException() {
        super("Error! Status is empty");
    }

    public EmptyStatusException(String message) {
        super("Error! " + message + " status is empty");
    }

}
