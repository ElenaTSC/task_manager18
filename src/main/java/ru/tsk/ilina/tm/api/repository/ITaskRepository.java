package ru.tsk.ilina.tm.api.repository;


import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskRepository {

    List<Task> findAllTaskByProjectId(String userId, String projectId);

    Task bindTaskToProjectById(String userId, String projectId, String taskId);

    Task unbindTaskToProjectById(String userId, String projectId, String taskId);

    void removeAllTaskByProjectId(String userId, String projectId);

    void add(String userId, Task task);

    void remove(String userId, Task task);

    void clear(String userId);

    Task startByID(String userId, String id);

    Task startByIndex(String userId, Integer index);

    Task startByName(String userId, String name);

    Task finishByID(String userId, String id);

    Task finishByIndex(String userId, Integer index);

    Task finishByName(String userId, String name);

    List<Task> findAll(String userId);

    List<Task> findAll(String userId, Comparator<Task> comparator);

    Task removeByID(String userId, String id);

    Task removeByIndex(String userId, Integer index);

    Task removeByName(String userId, String name);

    Task findByID(String userId, String id);

    Task findByIndex(String userId, Integer index);

    Task findByName(String userId, String name);

    Task changeStatusByID(String userId, String id, Status status);

    Task changeStatusByIndex(String userId, Integer index, Status status);

    Task changeStatusByName(String userId, String name, Status status);

    boolean existsById(String userId, String id);

    Integer getSize(String userId);

}
