package ru.tsk.ilina.tm.api.service;

import ru.tsk.ilina.tm.enumerated.Role;
import ru.tsk.ilina.tm.model.User;

public interface IUserService {

    User add(String login, String password);

    User add(String login, String password, String email);

    User add(String login, String password, Role role);

    User findById(String id);

    User findByEmail(String email);

    User setPassword(String userId, String password);

    User updateUser(String userId, String firstName, String lastName, String middleName);


    User removeById(String id);

    User removeByLogin(String login);

    User findByLogin(String login);

    User removeUser(User user);

    boolean isLoginExists(String login);
}
