package ru.tsk.ilina.tm.api.repository;

import ru.tsk.ilina.tm.model.User;

import java.util.List;

public interface IUserRepository {

    List<User> findAll();

    User add(User user);

    User findById(String id);

    User findByEmail(String email);

    User removeById(String id);

    User removeByLogin(String login);

    User findByLogin(String login);

    User removeUser(User user);

}
