package ru.tsk.ilina.tm.api.service;

import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    void create(String userId, String name, String description);

    void add(String userId, Task task);

    void remove(String userId, Task task);

    void clear(String userId);

    List<Task> findAll(String userId);

    List<Task> findAll(String userId, Comparator<Task> comparator);

    Task removeByID(String userId, String id);

    Task removeByIndex(String userId, Integer index);

    Task removeByName(String userId, String name);

    Task findByID(String userId, String id);

    Task findByIndex(String userId, Integer index);

    Task findByName(String userId, String name);

    Task updateById(String userId, String id, String name, String description);

    Task updateByIndex(String userId, Integer index, String name, String description);

    Task startByID(String userId, String id);

    Task startByIndex(String userId, Integer index);

    Task startByName(String userId, String name);

    Task finishByID(String userId, String id);

    Task finishByIndex(String userId, Integer index);

    Task finishByName(String userId, String name);

    Task changeStatusByIndex(String userId, Integer index, Status status);

    Task changeStatusByName(String userId, String name, Status status);

    Task changeStatusByID(String userId, String id, Status status);

}
