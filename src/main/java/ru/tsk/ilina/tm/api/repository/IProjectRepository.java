package ru.tsk.ilina.tm.api.repository;

import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.model.Project;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    Integer getSize(String userId);

    Project removeByID(String userId, String id);

    Project removeByIndex(String userId, Integer index);

    Project removeByName(String userId, String name);

    Project findByID(String userId, String id);

    Project findByIndex(String userId, Integer index);

    Project findByName(String userId, String name);

    Project startByID(String userId, String id);

    Project startByIndex(String userId, Integer index);

    Project startByName(String userId, String name);

    Project changeStatusByID(String userId, String id, Status status);

    Project changeStatusByIndex(String userId, Integer index, Status status);

    Project changeStatusByName(String userId, String name, Status status);

    Project finishByID(String userId, String id);

    Project finishByIndex(String userId, Integer index);

    Project finishByName(String userId, String name);

    void add(String userId, Project project);

    void remove(String userId, Project project);

    void clear(String userId);

    List<Project> findAll(String userId);

    List<Project> findAll(String userId, Comparator<Project> comparator);

    boolean existsById(String userId, String id);

}
