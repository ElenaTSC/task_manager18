package ru.tsk.ilina.tm.api.service;

import ru.tsk.ilina.tm.model.User;

public interface IAuthService {

    void login(String login, String password);

    void logout();

    void registry(String login, String password, String email);

    String getUserId();

    User getUser();

    void updateUser(String userId, String firstName, String lastName, String middleName);

    void setPassword(String userId, String password);

}
