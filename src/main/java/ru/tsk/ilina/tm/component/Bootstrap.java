package ru.tsk.ilina.tm.component;

import ru.tsk.ilina.tm.api.service.*;
import ru.tsk.ilina.tm.command.AbstractCommand;
import ru.tsk.ilina.tm.command.auth.*;
import ru.tsk.ilina.tm.command.project.*;
import ru.tsk.ilina.tm.command.project.ProjectTaskBindByIdCommand;
import ru.tsk.ilina.tm.command.project.ProjectTaskFindByIdCommand;
import ru.tsk.ilina.tm.command.project.ProjectTaskRemoveAllByIdCommand;
import ru.tsk.ilina.tm.command.project.ProjectTaskUnbindByIdCommand;
import ru.tsk.ilina.tm.command.system.*;
import ru.tsk.ilina.tm.command.task.*;
import ru.tsk.ilina.tm.enumerated.Role;
import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.exception.system.UnknowCommandException;
import ru.tsk.ilina.tm.model.Project;
import ru.tsk.ilina.tm.model.Task;
import ru.tsk.ilina.tm.repository.CommandRepository;
import ru.tsk.ilina.tm.repository.ProjectRepository;
import ru.tsk.ilina.tm.repository.TaskRepository;
import ru.tsk.ilina.tm.repository.UserRepository;
import ru.tsk.ilina.tm.service.*;

import java.util.Scanner;

public class Bootstrap implements IServiceLocator {

    private final UserRepository userRepository = new UserRepository();
    private final IUserService userService = new UserService(userRepository);
    private final IAuthService authService = new AuthService(userService);
    private final CommandRepository commandRepository = new CommandRepository();
    private final ICommandService commandService = new CommandService(commandRepository);
    private final TaskRepository taskRepository = new TaskRepository();
    private final ITaskService taskService = new TaskService(taskRepository);
    private final ProjectRepository projectRepository = new ProjectRepository();
    private final IProjectService projectService = new ProjectService(projectRepository);
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    private final ILogService logService = new LogService();

    public void start(String[] args) {
        logService.debug("Test environment");
        System.out.println("**WELCOME TO TASK MANAGER**");
        if (runArg(args)) System.exit(0);
        initUsers();
        initData();
        registration();
        final Scanner scanner = new Scanner(System.in);
        while (true) {
            try {
                System.out.println("ENTER COMMAND: ");
                String command = scanner.nextLine();
                logService.command(command);
                runCommand(command);
                logService.info("Completed");
            } catch (Exception e) {
                logService.error(e);
            }
        }
    }

    private void registration() {
        registry(new AuthLoginCommand());
        registry(new AuthLogoutCommand());
        registry(new AuthRegistryCommand());
        registry(new AuthChangePasswordCommand());
        registry(new AuthUpdateProfileCommand());
        registry(new AuthViewProfileCommand());
        registry(new HelpCommand());
        registry(new AboutCommand());
        registry(new InfoCommand());
        registry(new VersionCommand());
        registry(new ExitCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectChangeStatusByNameCommand());
        registry(new ProjectFinishByIdCommand());
        registry(new ProjectFinishByIndexCommand());
        registry(new ProjectFinishByNameCommand());
        registry(new ProjectListShowCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectRemoveByNameCommand());
        registry(new ProjectShowByIdCommand());
        registry(new ProjectShowByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectStartByNameCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskClearCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskChangeStatusByNameCommand());
        registry(new TaskFinishByIdCommand());
        registry(new TaskFinishByIndexCommand());
        registry(new TaskFinishByNameCommand());
        registry(new TaskListShowCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskRemoveByNameCommand());
        registry(new TaskShowByIdCommand());
        registry(new TaskShowByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskStartByNameCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());
        registry(new ProjectTaskFindByIdCommand());
        registry(new ProjectTaskBindByIdCommand());
        registry(new ProjectTaskUnbindByIdCommand());
        registry(new ProjectTaskRemoveAllByIdCommand());
    }

    private void registry(AbstractCommand command) {
        if (command == null) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void runCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new UnknowCommandException();
        abstractCommand.execute();
    }

    private boolean runArg(final String[] args) {
        if (args == null || args.length == 0) return false;
        AbstractCommand abstractCommand = commandService.getCommandByArg(args[0]);
        if (abstractCommand == null) throw new UnknowCommandException();
        abstractCommand.execute();
        return true;
    }

    private void initData() {
        Project project1 = new Project("Project C", "-");
        project1.setStatus(Status.COMPLETED);
        String userId = "test";
        projectService.add(userId, project1);
        Project project2 = new Project("Project A", "-");
        project2.setStatus(Status.IN_PROGRESS);
        projectService.add(userId, project2);
        Project project3 = new Project("Project B", "-");
        projectService.add(userId, project3);
        Task task1 = new Task("Task B", "-");
        task1.setStatus(Status.COMPLETED);
        taskService.add(userId, task1);
        Task task2 = new Task("Task A", "-");
        task2.setStatus(Status.COMPLETED);
        taskService.add(userId, task2);
        Task task3 = new Task("Task C", "-");
        task3.setStatus(Status.COMPLETED);
        taskService.add(userId, task3);
    }

    private void initUsers() {
        userService.add("test", "test", "test@test.com");
        userService.add("admin", "admin", Role.ADMIN);
    }

    @Override
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    public IAuthService getAuthService() {
        return authService;
    }

}
