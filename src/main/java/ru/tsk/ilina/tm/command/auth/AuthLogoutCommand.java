package ru.tsk.ilina.tm.command.auth;

import ru.tsk.ilina.tm.command.AbstractAuthUserCommand;
import ru.tsk.ilina.tm.util.TerminalUtil;

public class AuthLogoutCommand extends AbstractAuthUserCommand {

    @Override
    public String name() {
        return "logout";
    }

    @Override
    public String description() {
        return "Logout in system";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public void execute() {
        System.out.println("[LOGOUT]");
        serviceLocator.getAuthService().logout();
        System.out.println("[OK]");
    }

}
