package ru.tsk.ilina.tm.command.system;

import ru.tsk.ilina.tm.command.AbstractCommand;

public class AboutCommand extends AbstractCommand {

    @Override
    public String name() {
        return "about";
    }

    @Override
    public String description() {
        return "Display developer info.";
    }

    @Override
    public String arg() {
        return "-a";
    }

    @Override
    public void execute() {
        System.out.println("[ABOUT]");
        System.out.println("DEVELOPER: Elena Ilina");
        System.out.println("E-MAIL: eilina@tsconsulting.com");
    }

}
