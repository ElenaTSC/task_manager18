package ru.tsk.ilina.tm.service;

import ru.tsk.ilina.tm.api.repository.IUserRepository;
import ru.tsk.ilina.tm.api.service.IUserService;
import ru.tsk.ilina.tm.enumerated.Role;
import ru.tsk.ilina.tm.exception.empty.EmptyEmailException;
import ru.tsk.ilina.tm.exception.empty.EmptyIdException;
import ru.tsk.ilina.tm.exception.empty.EmptyLoginException;
import ru.tsk.ilina.tm.exception.empty.EmptyPasswordException;
import ru.tsk.ilina.tm.exception.user.LoginExistsException;
import ru.tsk.ilina.tm.model.User;
import ru.tsk.ilina.tm.util.HashUtil;

public class UserService implements IUserService {

    private final IUserRepository userRepository;

    public UserService(IUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public User add(final String login, final String password) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = new User();
        user.setRole(Role.USER);
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(password));
        return userRepository.add(user);
    }

    @Override
    public User add(final String login, final String password, final String email) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        if (isLoginExists(login)) throw new LoginExistsException();
        final User user = add(login, password);
        if (user == null) return null;
        user.setEmail(email);
        return user;
    }

    @Override
    public User add(final String login, final String password, final Role role) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        if (isLoginExists(login)) throw new LoginExistsException();
        if (role == null) return null;
        final User user = add(login, password);
        if (user == null) return null;
        user.setRole(role);
        return user;
    }

    @Override
    public User findById(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        return userRepository.findById(userId);
    }

    @Override
    public User findByEmail(String email) {
        if (email == null || email.isEmpty()) throw new EmptyEmailException();
        return userRepository.findByEmail(email);
    }

    @Override
    public User setPassword(final String userId, final String password) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (password == null || password.isEmpty()) throw new EmptyPasswordException();
        final User user = findById(userId);
        if (user == null) return null;
        final String hash = HashUtil.salt(password);
        user.setPasswordHash(hash);
        return user;
    }

    @Override
    public User updateUser(final String userId,
                           final String firstName,
                           final String lastName,
                           final String middleName
    ) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        final User user = findById(userId);
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        return user;
    }

    @Override
    public User removeById(final String userId) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        return userRepository.removeById(userId);
    }

    @Override
    public User removeByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.removeByLogin(login);
    }

    @Override
    public User findByLogin(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return userRepository.findByLogin(login);
    }

    @Override
    public User removeUser(final User user) {
        if (user == null) return null;
        return userRepository.removeUser(user);
    }

    @Override
    public boolean isLoginExists(final String login) {
        if (login == null || login.isEmpty()) throw new EmptyLoginException();
        return findByLogin(login) != null;
    }

}
