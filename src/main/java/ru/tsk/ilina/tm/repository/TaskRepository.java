package ru.tsk.ilina.tm.repository;

import ru.tsk.ilina.tm.api.repository.ITaskRepository;
import ru.tsk.ilina.tm.enumerated.Status;
import ru.tsk.ilina.tm.model.Project;
import ru.tsk.ilina.tm.model.Task;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class TaskRepository implements ITaskRepository {

    private final List<Task> tasks = new ArrayList<>();

    @Override
    public void add(final String userId, Task task) {
        task.setUserId(userId);
        tasks.add(task);
    }

    @Override
    public void remove(final String userId, Task task) {
        if (!userId.equals(task.getUserId())) return;
        tasks.remove(task);
    }

    @Override
    public void clear(final String userId) {
        final List<Task> taskList = new ArrayList<>();
        for (final Task task : tasks) {
            if (userId.equals(task.getUserId())) taskList.add(task);
        }
        taskList.clear();
    }

    @Override
    public Task startByID(final String userId, String id) {
        final Task task = findByID(userId, id);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByIndex(final String userId, Integer index) {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task startByName(final String userId, String name) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(Status.IN_PROGRESS);
        return task;
    }

    @Override
    public Task finishByID(final String userId, String id) {
        final Task task = finishByID(userId, id);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByIndex(final String userId, Integer index) {
        final Task task = finishByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public Task finishByName(final String userId, String name) {
        final Task task = finishByName(userId, name);
        if (task == null) return null;
        task.setStatus(Status.COMPLETED);
        return task;
    }

    @Override
    public List<Task> findAll(final String userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result;
    }

    @Override
    public Task removeByID(final String userId, final String id) {
        final Task task = findByID(userId, id);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByIndex(final String userId, final Integer index) {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task removeByName(final String userId, final String name) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        tasks.remove(task);
        return task;
    }

    @Override
    public Task findByID(final String userId, final String id) {
        for (Task task : tasks) {
            if (!userId.equals(task.getUserId())) continue;
            if (id.equals(task.getId())) return task;
        }
        return null;
    }

    @Override
    public Task findByIndex(final String userId, final Integer index) {
        final List<Task> task = findAll(userId);
        return task.get(index);
    }

    @Override
    public Task findByName(final String userId, final String name) {
        for (Task task : tasks) {
            if (!userId.equals(task.getUserId())) continue;
            if (name.equals(task.getName())) return task;
        }
        return null;
    }

    @Override
    public Task changeStatusByID(final String userId, String id, Status status) {
        final Task task = findByID(userId, id);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByIndex(final String userId, Integer index, Status status) {
        final Task task = findByIndex(userId, index);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public Task changeStatusByName(final String userId, String name, Status status) {
        final Task task = findByName(userId, name);
        if (task == null) return null;
        task.setStatus(status);
        return task;
    }

    @Override
    public boolean existsById(final String userId, final String id) {
        return findByID(userId, id) != null;
    }

    @Override
    public Task bindTaskToProjectById(final String userId, String projectId, String taskId) {
        final Task task = findByID(userId, taskId);
        if (task == null) return null;
        task.setProjectId(projectId);
        return task;
    }

    @Override
    public List<Task> findAllTaskByProjectId(final String userId, String projectId) {
        List<Task> taskProject = new ArrayList<Task>();
        for (Task task : tasks) {
            String id = task.getProjectId();
            if (userId.equals(task.getUserId())) continue;
            ;
            if (projectId.equals(id)) {
                taskProject.add(task);
            }
        }
        return taskProject;
    }

    @Override
    public Task unbindTaskToProjectById(final String userId, String projectId, String taskId) {
        final Task task = findByID(userId, taskId);
        if (task == null) return null;
        task.setProjectId(null);
        return task;
    }

    @Override
    public void removeAllTaskByProjectId(final String userId, String projectId) {
        List<Task> listByProject = findAllTaskByProjectId(userId, projectId);
        for (Task task : listByProject) {
            tasks.remove(task);
        }
    }

    @Override
    public List<Task> findAll(final String userId, Comparator<Task> comparator) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        result.sort(comparator);
        return result;
    }

    @Override
    public Integer getSize(final String userId) {
        final List<Task> result = new ArrayList<>();
        for (final Task task : tasks) {
            if (userId.equals(task.getUserId())) result.add(task);
        }
        return result.size();
    }

}
